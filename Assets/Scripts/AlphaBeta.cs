﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaBeta
{
    public Tile[,] board;
    public Player redPlayer;
    public Player blackPlayer;
    public bool isRed;
    public Tuple<int, int> kaitoLocation;
    public int depthLimit = 36;
    public bool Sort = false;

    public Tuple<int, int> AlphaBetaSearch()
    {
        Player redPlayerState = redPlayer.Clone();
        Player blackPlayerState = blackPlayer.Clone();
        redPlayerState.Score = 0;
        blackPlayerState.Score = 0;
        Tile[,] boardState = DeepClone(board, redPlayerState, blackPlayerState);
        Tuple<int, int> newKaitoLocation;
        if (kaitoLocation != null)
        {
            newKaitoLocation = new Tuple<int, int>(kaitoLocation.Item1, kaitoLocation.Item2);
        } else
        {
            newKaitoLocation = null;
        }
         
        Tuple<Tuple<int, int>, double> result = maxValue(boardState, redPlayerState, blackPlayerState, newKaitoLocation, double.NegativeInfinity, double.PositiveInfinity, 0);
        return result.Item1;
    }

    private Tuple<Tuple<int, int>, double> maxValue(Tile[,] boardState, Player redPlayerState, Player blackPlayerState, Tuple<int, int> newKaitoLocation, double alpha, double beta, int depth)
    {
        Tuple<Tuple<int, int>, double> result = new Tuple<Tuple<int, int>, double>(null, double.NegativeInfinity);
        //Debug.Log("Depth");
        //If Terminal
        if (isRed)
        {
            if (redPlayerState.Helmets == 0 || redPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, -100000 + depth);
            }

            if (blackPlayerState.Helmets == 0 || blackPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, 100000 - depth);
            }

            if (depth == depthLimit)
            {
                return new Tuple<Tuple<int, int>, double>(null, redPlayerState.Score - blackPlayerState.Score);
            }
        }
        else
        {
            if (redPlayerState.Helmets == 0 || redPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, 100000 - depth);
            }

            if (blackPlayerState.Helmets == 0 || blackPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, -100000 + depth);
            }

            if (depth == depthLimit)
            {
                return new Tuple<Tuple<int, int>, double>(null, blackPlayerState.Score - redPlayerState.Score);
            }
        }

        Player maxPlayer;
        if (isRed)
        {
            maxPlayer = redPlayerState;
        } else
        {
            maxPlayer = blackPlayerState;
        }

        List<Tuple<int, int>> moves = getPossibleActions(boardState, maxPlayer, newKaitoLocation);
        if (moves.Count == 0)
        {
            if (isRed)
            {
                return new Tuple<Tuple<int, int>, double>(null, redPlayerState.Score - blackPlayerState.Score);
            }
            else
            {
                return new Tuple<Tuple<int, int>, double>(null, blackPlayerState.Score - redPlayerState.Score);
            }
        }

        foreach (Tuple<int,int> move  in moves)
        {
            Player redPlayerStateAfter = redPlayerState.Clone();
            Player blackPlayerStateAfter = blackPlayerState.Clone();
            Tile[,] boardStateAfter = DeepClone(boardState, redPlayerStateAfter, blackPlayerStateAfter);
            makeMove(boardStateAfter, redPlayerStateAfter, blackPlayerStateAfter, move, newKaitoLocation, isRed);
            Tuple<Tuple<int, int>, double> resultMin;
            if (move.Item1 < 0)
            {
                resultMin = minValue(boardStateAfter, redPlayerStateAfter, blackPlayerStateAfter, newKaitoLocation, alpha, beta, depth + 1);
            }
            else
            {
                resultMin = minValue(boardStateAfter, redPlayerStateAfter, blackPlayerStateAfter, move, alpha, beta, depth + 1);
            }
            if (resultMin.Item2 > result.Item2)
            {
                result = new Tuple<Tuple<int, int>, double>(move, resultMin.Item2);
                alpha = Math.Max(alpha, result.Item2);
            }

            if (result.Item2 > beta)
            {
                return new Tuple<Tuple<int, int>, double>(move, result.Item2);
            }
        }

        return result;
    }

    private Tuple<Tuple<int, int>, double> minValue(Tile[,] boardState, Player redPlayerState, Player blackPlayerState, Tuple<int, int> newKaitoLocation, double alpha, double beta, int depth)
    {
        Tuple<Tuple<int, int>, double> result = new Tuple<Tuple<int, int>, double>(newKaitoLocation, double.PositiveInfinity);
        //If Terminal
        //test
        if (isRed)
        {
            if (redPlayerState.Helmets == 0 || redPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, -100000 + depth);
            }

            if (blackPlayerState.Helmets == 0 || blackPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, 100000 - depth);
            }

            if (depth == depthLimit)
            {
                return new Tuple<Tuple<int, int>, double>(null, redPlayerState.Score - blackPlayerState.Score);
            }
        }
        else
        {
            if (redPlayerState.Helmets == 0 || redPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, 100000 - depth);
            }

            if (blackPlayerState.Helmets == 0 || blackPlayerState.Swords == 0)
            {
                return new Tuple<Tuple<int, int>, double>(null, -100000 + depth);
            }

            if (depth == depthLimit)
            {
                return new Tuple<Tuple<int, int>, double>(null, blackPlayerState.Score - redPlayerState.Score);
            }
        }

        Player minPlayer;
        if (isRed)
        {
            minPlayer = blackPlayerState;
        }
        else
        {
            minPlayer = redPlayerState;
        }

        List<Tuple<int, int>> moves = getPossibleActions(boardState, minPlayer, newKaitoLocation);
        if (moves.Count == 0)
        {
            if (isRed)
            {
                return new Tuple<Tuple<int, int>, double>(null, redPlayerState.Score);
            } else
            {
                return new Tuple<Tuple<int, int>, double>(null, blackPlayerState.Score);
            }
        }

        foreach (Tuple<int, int> move in moves)
        {
            Player redPlayerStateAfter = redPlayerState.Clone();
            Player blackPlayerStateAfter = blackPlayerState.Clone();
            Tile[,] boardStateAfter = DeepClone(boardState, redPlayerStateAfter, blackPlayerStateAfter);
            makeMove(boardStateAfter, redPlayerStateAfter, blackPlayerStateAfter, move, newKaitoLocation, !isRed);
            Tuple<Tuple<int, int>, double> resultMin;
            if (move.Item1 < 0)
            {
                resultMin = maxValue(boardStateAfter, redPlayerStateAfter, blackPlayerStateAfter, newKaitoLocation, alpha, beta, depth + 1);
            } else
            {
                resultMin = maxValue(boardStateAfter, redPlayerStateAfter, blackPlayerStateAfter, move, alpha, beta, depth + 1);
            }
            
            if (resultMin.Item2 < result.Item2)
            {
                result = new Tuple<Tuple<int, int>, double>(move, resultMin.Item2);
                beta = Math.Min(beta, result.Item2);
            }

            if (result.Item2 < alpha)
            {
                return new Tuple<Tuple<int, int>, double>(move, result.Item2);
            }
        }

        return result;
    }

    private Tile[,] DeepClone (Tile[,] boardState, Player redPlayerState, Player blackPlayerState)
    {
        Tile[,] newBoardState = new Tile[6, 6];
        for(int i = 0; i<6; i++)
        {
            for(int j = 0; j<6; j++)
            {
                Tile oldTile = boardState[i,j];
                Tile newTile;
                if (oldTile.owner != null)
                {
                    if (oldTile.owner.isRed)
                    {
                        newTile = new Tile(oldTile.tileType, redPlayerState);
                    }
                    else
                    {
                        newTile = new Tile(oldTile.tileType, blackPlayerState);
                    }
                } else
                {
                    newTile = new Tile(oldTile.tileType, null);
                }

                newBoardState[i, j] = newTile;
            }
        }
        return newBoardState;
    }

    private List<Tuple<int,int>> getPossibleActions(Tile[,] boardState, Player playerState, Tuple<int, int> newKaitoLocation)
    {
        List<Tuple<int, int>> moves = new List<Tuple<int, int>>();

        if (newKaitoLocation == null)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (boardState[i, j].tileType == TileEnum.Blank)
                    {
                        moves.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            return moves;
        }

        for (int i = newKaitoLocation.Item1 + 1; i < 6; i++)
        {
            if (boardState[i, newKaitoLocation.Item2].tileType != TileEnum.Empty)
            {
                moves.Add(new Tuple<int, int>(i, newKaitoLocation.Item2));
            }
        }

        for (int i = newKaitoLocation.Item1 - 1; i >= 0; i--)
        {
            if (boardState[i, newKaitoLocation.Item2].tileType != TileEnum.Empty)
            {
                moves.Add(new Tuple<int, int>(i, newKaitoLocation.Item2));
            }
        }

        for (int j = newKaitoLocation.Item2 + 1; j < 6; j++)
        {
            if (boardState[newKaitoLocation.Item1, j].tileType != TileEnum.Empty)
            {
                moves.Add(new Tuple<int, int>(newKaitoLocation.Item1, j));
            }
        }

        for (int j = newKaitoLocation.Item2 - 1; j >= 0; j--)
        {
            if (boardState[newKaitoLocation.Item1, j].tileType != TileEnum.Empty)
            {
                moves.Add(new Tuple<int, int>(newKaitoLocation.Item1, j));
            }
        }

        if (playerState.CoinsCollected >= 5 && playerState.Helmets < 3 && boardState[newKaitoLocation.Item1, newKaitoLocation.Item2].tileType == TileEnum.Empty)
        {
            moves.Add(new Tuple<int, int>(-2, -2));
        }

        if (playerState.CoinsCollected >= 4 && playerState.Swords < 7 && boardState[newKaitoLocation.Item1, newKaitoLocation.Item2].tileType == TileEnum.Empty)
        {
            moves.Add(new Tuple<int, int>(-1, -1));
        }


        //TODO:Buy back
        if (Sort)
        {
            if (playerState.isRed)
            {
                moves.Sort((x, y) =>
                {
                    if (x.Item1 >= 0 && y.Item1 >= 0)
                    {
                        return Tile.GetOrderRed(boardState[x.Item1, x.Item2].tileType) - Tile.GetOrderRed(boardState[y.Item1, y.Item2].tileType);
                    }

                    return x.Item1 < 0 ? -1 : 1;
                });
            }
            else
            {
                moves.Sort((x, y) =>
                {
                    if (x.Item1 >= 0 && y.Item1 >= 0)
                    {
                        return Tile.GetOrderBlack(boardState[x.Item1, x.Item2].tileType) - Tile.GetOrderBlack(boardState[y.Item1, y.Item2].tileType);
                    }

                    return x.Item1 < 0 ? -1 : 1;
                });
            }
        }

        return moves;
    }

    public void makeMove(Tile[,] boardState, Player redPlayerState, Player blackPlayerState, Tuple<int, int> action, Tuple<int, int> newKaitoLocation, bool isRed)
    {
        if (isRed)
        {
            if (action.Item1 == -1)
            {
                boardState[newKaitoLocation.Item1, newKaitoLocation.Item2] = new Tile(TileEnum.Sword_R, redPlayerState);
                redPlayerState.Swords++;
                redPlayerState.CoinsCollected -= 4;
                redPlayerState.Score += 50;
            }  else if (action.Item1 == -2)
            {
                boardState[newKaitoLocation.Item1, newKaitoLocation.Item2] = new Tile(TileEnum.Helmet_R, redPlayerState);
                redPlayerState.Helmets++;
                redPlayerState.CoinsCollected -= 5;
                redPlayerState.Score += 100;
            } else
            {
                boardState[action.Item1, action.Item2].Collect(redPlayerState, blackPlayerState);
            }
        } else
        {
            if (action.Item1 == -1)
            {
                boardState[newKaitoLocation.Item1, newKaitoLocation.Item2] = new Tile(TileEnum.Sword_B, blackPlayerState);
                blackPlayerState.Swords++;
                blackPlayerState.CoinsCollected -= 4;
                blackPlayerState.Score += 40;
            }
            else if (action.Item1 == -2)
            {
                boardState[newKaitoLocation.Item1, newKaitoLocation.Item2] = new Tile(TileEnum.Helmet_B, blackPlayerState);
                blackPlayerState.Helmets++;
                blackPlayerState.CoinsCollected -= 5;
                blackPlayerState.Score += 120;
            }
            else
            {
                boardState[action.Item1, action.Item2].Collect(blackPlayerState, redPlayerState);
            }
        }

        //boardState[action.Item1, action.Item2] = new Tile(TileEnum.Empty, null);
    }
}
