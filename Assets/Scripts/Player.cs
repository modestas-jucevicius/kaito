﻿using System.Collections;
using System.Collections.Generic;

public class Player
{
    private int swords = 7;
    private int helmets = 3;
    private int coinsCollected = 0;
    private double score = 0;
    public bool isRed = false;
    public int Swords { get => swords; set => swords = value; }
    public int Helmets { get => helmets; set => helmets = value; }
    public int CoinsCollected { get => coinsCollected; set => coinsCollected = value; }
    public double Score { get => score; set => score = value; }

    public Player(bool isRed)
    {
        this.isRed = isRed;
    }

    public Player Clone()
    {
        Player clone = new Player(isRed);
        clone.Swords = this.swords;
        clone.Helmets = this.helmets;
        clone.CoinsCollected = this.coinsCollected;
        clone.Score = this.score;
        return clone;
    }
}
