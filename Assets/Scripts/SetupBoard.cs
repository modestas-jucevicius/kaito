﻿using System;
using UnityEngine;
using Random = System.Random;
using UnityEngine.SceneManagement;
using System.Threading;
using UnityEngine.UI;

public class SetupBoard : MonoBehaviour
{
	public GameObject tilePrefab;
	public GameObject kaito;
	public readonly Player redPlayer = new Player(true);
	public readonly Player blackPlayer = new Player(false);
	public bool RedTurn = false;
	private static readonly float GAP_SIZE = 0.0f;
	public GameObject[,] board;
	public GameObject GameOver;
	public GameObject RedUI;
	public GameObject BlackUI;
	public GameObject LoadUI;
	public GameObject ScoreRUI;
	public GameObject ScoreBUI;
	public GameObject CoinsRUI;
	public GameObject CoinsBUI;
	public bool AI = false;
	public int AIDepth = 32;
	public bool AISort = false;
	public bool AI2 = false;
	public int AI2Depth = 32;
	public bool AI2Sort = false;
	private Tuple<int, int> kaitoLocation = null;
	private AlphaBeta alphaBeta;
	private AlphaBeta alphaBeta2;
	private bool AISearch = false;
	private Tuple<int, int> AIFoundMove = null;
	private bool gameOver = false;
	public Tuple<int, int> KaitoLocation {
		get
		{
			return kaitoLocation;
        }
		set
        {
			if(AI)
            {
				alphaBeta.kaitoLocation = value;
			}

			if(AI2)
            {
				alphaBeta2.kaitoLocation = value;
			}
			kaitoLocation = value;
			UpdateKaito();
		}
	}
	// Start is called before the first frame update
    void Start()
    {
		BLANKS = 2;
		SWORD_B = 7;
		SWORD_R = 7;
		HELMET_B = 3;
		HELMET_R = 3;
		COIN_1_B = 3;
		COIN_1_R = 3;
		COIN_2_B = 2;
		COIN_2_R = 2;
		COIN_3_B = 2;
		COIN_3_R = 2;
		REMAINING = BLANKS + SWORD_B + SWORD_R + HELMET_B + HELMET_R + COIN_1_B + COIN_1_R + COIN_2_B + COIN_2_R + COIN_3_B + COIN_3_R;
		board = new GameObject[6, 6];
		float tileDistance = GAP_SIZE + tilePrefab.transform.localScale.x * 1.5f;
		float startPosition_x = tileDistance * -6/2 + tileDistance/2;
		float startPosition_y = tileDistance * 6/2 - tileDistance/2;
        for (int x = 0 ; x < 6; x++) {
			for (int y = 0; y < 6; y++) {
				TileEnum tileEnum = RandomizeTile();
				Tile tile;
				if (Tile.IsTileRed(tileEnum))
                {
					tile = new Tile(tileEnum, redPlayer);
				} else if (Tile.IsTileBlack(tileEnum))
                {
					tile = new Tile(tileEnum, blackPlayer);
                } else
                {
					tile = new Tile(tileEnum, null);
                }

				GameObject newTile = Instantiate(tilePrefab, new Vector3(startPosition_x + x * tileDistance, startPosition_y - y * tileDistance, tilePrefab.transform.position.z), Quaternion.Euler(0, 0, 180));
				TileUI tileUI = newTile.GetComponent<TileUI>();
				tileUI.board = this;
				tileUI.tile = tile;
				tileUI.tileCoords = new Tuple<int, int>(x, y);
				board[x, y] = newTile;
			}
		}
		
		if(AI)
        {
			alphaBeta = new AlphaBeta();
			alphaBeta.blackPlayer = blackPlayer;
			alphaBeta.redPlayer = redPlayer;
			alphaBeta.isRed = true;
			Tile[,] aiBoard = new Tile[6, 6];
			for (int x = 0; x < 6; x++)
			{
				for (int y = 0; y < 6; y++)
				{
					aiBoard[x, y] = board[x, y].GetComponent<TileUI>().tile;
				}
			}
			alphaBeta.board = aiBoard;
			alphaBeta.depthLimit = AIDepth;
			alphaBeta.Sort = AISort;
		}

		if (AI2)
		{
			alphaBeta2 = new AlphaBeta();
			alphaBeta2.blackPlayer = blackPlayer;
			alphaBeta2.redPlayer = redPlayer;
			alphaBeta2.isRed = false;
			Tile[,] aiBoard = new Tile[6, 6];
			for (int x = 0; x < 6; x++)
			{
				for (int y = 0; y < 6; y++)
				{
					aiBoard[x, y] = board[x, y].GetComponent<TileUI>().tile;
				}
			}
			alphaBeta2.board = aiBoard;
			alphaBeta2.depthLimit = AI2Depth;
			alphaBeta2.Sort = AI2Sort;
		}
	}

	public void Restart()
    {
		BLANKS = 2;
		SWORD_B = 7;
		SWORD_R = 7;
		HELMET_B = 3;
		HELMET_R = 3;
		COIN_1_B = 3;
		COIN_1_R = 3;
		COIN_2_B = 2;
		COIN_2_R = 2;
		COIN_3_B = 2;
		COIN_3_R = 2;
		REMAINING = BLANKS + SWORD_B + SWORD_R + HELMET_B + HELMET_R + COIN_1_B + COIN_1_R + COIN_2_B + COIN_2_R + COIN_3_B + COIN_3_R;
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	private void UpdateKaito()
    {
		kaito.transform.position = board[kaitoLocation.Item1, kaitoLocation.Item2].transform.position;
		kaito.transform.RotateAround(kaito.transform.position, Vector3.up, 180);
		RedTurn = !RedTurn;
		if (redPlayer.Helmets == 0 || redPlayer.Swords == 0 )
        {
			GameOver.SetActive(true);
			BlackUI.SetActive(true);
		}

		if (blackPlayer.Helmets == 0 || blackPlayer.Swords == 0)
        {
			GameOver.SetActive(true);
			RedUI.SetActive(true);
		}

		ScoreBUI.GetComponent<Text>().text = "Score: " + blackPlayer.Score;
		ScoreRUI.GetComponent<Text>().text = "Score: " + redPlayer.Score;
		CoinsBUI.GetComponent<Text>().text = "Coins: " + blackPlayer.CoinsCollected;
		CoinsRUI.GetComponent<Text>().text = "Coins: " + redPlayer.CoinsCollected;
	}

    // Update is called once per frame
    void Update()
    {
		if (gameOver)
		{
			GameOver.SetActive(true);
			if (RedTurn)
			{
				BlackUI.SetActive(true);
			}
			else
			{
				RedUI.SetActive(true);
			}
		}

		if (AISearch)
        {
			LoadUI.SetActive(true);
			LoadUI.transform.Rotate(Vector3.forward, -1);
        } else
        {
			LoadUI.SetActive(false);
		}
    }

	void FixedUpdate()
	{
		if (Input.GetMouseButtonDown(0))
        {
			if (!AI2 && !RedTurn && !AISearch)
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit, 300))
				{
					GameObject target = hit.collider.gameObject;
					TileUI tileUI = target.GetComponent<TileUI>();
					if (tileUI != null)
					{
						if (RedTurn)
						{
							Debug.Log("RedCollects");
							tileUI.Collect(redPlayer, blackPlayer);
						}
						else
						{
							Debug.Log("BlackCollects");
							tileUI.Collect(blackPlayer, redPlayer);
						}
					}
				}
			}

			if (AI && RedTurn && !AISearch)
            {
				AISearch = true;
				Thread AiThread = new Thread(() => {
					AIFoundMove = alphaBeta.AlphaBetaSearch();
					if (AIFoundMove == null)
                    {
						gameOver = true;
					}
				});
				AiThread.Start();
			}

			if (AI2 && !RedTurn && !AISearch)
			{
				AISearch = true;
				Thread AiThread = new Thread(() => {
					AIFoundMove = alphaBeta2.AlphaBetaSearch();
					if (AIFoundMove == null)
					{
						gameOver = true;
					}
				});
				AiThread.Start();
			}
		}

		if(AIFoundMove != null)
        {
			if (AI && RedTurn)
			{
				AIMove(alphaBeta, AIFoundMove);
			} else if (AI2 && !RedTurn)
            {
				AIMove(alphaBeta2, AIFoundMove);
			}
		}
	}

	public void BuyBackSword()
    {
		if (this.blackPlayer.CoinsCollected >= 4 && !RedTurn && board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().tile.tileType == TileEnum.Empty)
		{
			board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().BringBackSword(this.blackPlayer);
		}
	}

	public void BuyBackHelmet()
	{
		if (this.blackPlayer.CoinsCollected >= 5 && !RedTurn && board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().tile.tileType == TileEnum.Empty)
		{
			board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().BringBackHelmet(this.blackPlayer);
		}
	}

	private void AIMove(AlphaBeta alphaBeta, Tuple<int, int> move)
    {
        if (move.Item1 == -1 && alphaBeta.isRed)
        {
            board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().BringBackSword(redPlayer);
        }
        else if (move.Item1 == -1)
        {
            board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().BringBackSword(blackPlayer);
        }
        else if (move.Item1 == -2 && alphaBeta.isRed)
        {
            board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().BringBackHelmet(redPlayer);
        }
        else if (move.Item1 == -2)
        {
            board[kaitoLocation.Item1, kaitoLocation.Item2].GetComponent<TileUI>().BringBackHelmet(blackPlayer);
        }
        else
        {
            GameObject AItarget = board[move.Item1, move.Item2];
            TileUI AITileUI = AItarget.GetComponent<TileUI>();
            if (alphaBeta.isRed)
            {
                Debug.Log("RedCollects");
                AITileUI.Collect(redPlayer, blackPlayer);
            }
            else
            {
                Debug.Log("BlackCollects");
                AITileUI.Collect(blackPlayer, redPlayer);
            }
        }

        Tile[,] aiBoard = new Tile[6, 6];
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 6; y++)
            {
                aiBoard[x, y] = board[x, y].GetComponent<TileUI>().tile;
            }
        }
        alphaBeta.board = aiBoard;

		AISearch = false;
		AIFoundMove = null;
    }

    private static int BLANKS = 16;
	private static int SWORD_B = 7;
	private static int SWORD_R = 7;
	private static int HELMET_B = 3;
	private static int HELMET_R = 3;
	private static int COIN_1_B = 0;
	private static int COIN_1_R = 0;
	private static int COIN_2_B = 0;
	private static int COIN_2_R = 0;
	private static int COIN_3_B = 0;
	private static int COIN_3_R = 0;
	private static int REMAINING = BLANKS + SWORD_B + SWORD_R + HELMET_B + HELMET_R + COIN_1_B + COIN_1_R + COIN_2_B + COIN_2_R + COIN_3_B + COIN_3_R;
	private static readonly Random RANDOM = new Random();
	private TileEnum RandomizeTile()
    {
		int random = RANDOM.Next(0, REMAINING);
		int decider = BLANKS;
		if (random < decider)
        {
			BLANKS--;
			REMAINING--;
			return TileEnum.Blank;
        }

		decider += SWORD_B;
		if (random < decider)
		{
			SWORD_B--;
			REMAINING--;
			return TileEnum.Sword_B;
		}

		decider += SWORD_R;
		if (random < decider)
		{
			SWORD_R--;
			REMAINING--;
			return TileEnum.Sword_R;
		}

		decider += HELMET_B;
		if (random < decider)
		{
			HELMET_B--;
			REMAINING--;
			return TileEnum.Helmet_B;
		}

		decider += HELMET_R;
		if (random < decider)
		{
			HELMET_R--;
			REMAINING--;
			return TileEnum.Helmet_R;
		}

		decider += COIN_1_B;
		if (random < decider)
		{
			COIN_1_B--;
			REMAINING--;
			return TileEnum.Coin_1_B;
		}

		decider += COIN_1_R;
		if (random < decider)
		{
			COIN_1_R--;
			REMAINING--;
			return TileEnum.Coin_1_R;
		}

		decider += COIN_2_B;
		if (random < decider)
		{
			COIN_2_B--;
			REMAINING--;
			return TileEnum.Coin_2_B;
		}

		decider += COIN_2_R;
		if (random < decider)
		{
			COIN_2_R--;
			REMAINING--;
			return TileEnum.Coin_2_R;
		}

		decider += COIN_3_B;
		if (random < decider)
		{
			COIN_3_B--;
			REMAINING--;
			return TileEnum.Coin_3_B;
		}

		decider += COIN_3_R;
		if (random < decider)
		{
			COIN_3_R--;
			REMAINING--;
			return TileEnum.Coin_3_R;
		}

		return TileEnum.Blank;
	}
}
