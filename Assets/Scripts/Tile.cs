﻿public class Tile
{
    public TileEnum tileType;
    public Player owner;

    public Tile (TileEnum tileType, Player owner)
    {
        this.tileType = tileType;
        this.owner = owner;
    }   
    
    public void Collect(Player collector, Player enemy)
    {
		switch (tileType)
		{
			case TileEnum.Blank:
				tileType = TileEnum.Empty;
				owner = null;
				break;
			case TileEnum.Sword_R:
			case TileEnum.Sword_B:
				if (owner != collector)
                {
					collector.Score += 60;
				} else
                {
					collector.Score -= 60;
				}
				

				owner.Swords--;
				tileType = TileEnum.Empty;
				owner = null;
				break;

			case TileEnum.Helmet_R:
			case TileEnum.Helmet_B:
				if (owner != collector)
				{
					collector.Score += 140;
				}
				else
				{
					collector.Score -= 140;
				}

				owner.Helmets--;
				tileType = TileEnum.Empty;
				owner = null;
				break;

			case TileEnum.Coin_1_R:
			case TileEnum.Coin_1_B:
				if (collector == owner)
                {
					collector.CoinsCollected++;
					collector.Score += 21;
				}
				else
				{
					collector.Score += 10;
				}

				tileType = TileEnum.Empty;
				owner = null;
				break;

			case TileEnum.Coin_2_R:
			case TileEnum.Coin_2_B:
				if (collector == owner)
				{
					collector.CoinsCollected +=2;
					collector.Score += 42;
				}
				else
				{
					collector.Score += 20;
				}

				tileType = TileEnum.Empty;
				owner = null;
				break;
			case TileEnum.Coin_3_R:
			case TileEnum.Coin_3_B:
				if (collector == owner)
				{
					collector.CoinsCollected += 3;
					collector.Score += 63;
				}
				else
				{
					collector.Score += 30;
				}

				tileType = TileEnum.Empty;
				owner = null;
				break;
			default:
				break;
		}
	}

	public static bool IsTileRed(TileEnum tileType)
    {
        switch (tileType)
		{
			case TileEnum.Sword_R:
			case TileEnum.Helmet_R:
			case TileEnum.Coin_1_R:
			case TileEnum.Coin_2_R:
			case TileEnum.Coin_3_R:
				return true;
			default:
				return false;
		}
	}

	public static bool IsTileBlack(TileEnum tileType)
	{
		switch (tileType)
		{
			case TileEnum.Sword_B:
			case TileEnum.Helmet_B:
			case TileEnum.Coin_1_B:
			case TileEnum.Coin_2_B:
			case TileEnum.Coin_3_B:
				return true;
			default:
				return false;
		}
	}

	public static int GetOrderBlack(TileEnum tileType)
	{
		switch (tileType)
		{
			case TileEnum.Helmet_R:
				return -5;
			case TileEnum.Sword_R:
				return -4;
			case TileEnum.Coin_3_R:
				return -3;
			case TileEnum.Coin_2_R:
				return -2;
			case TileEnum.Coin_1_R:
				return -1;
			case TileEnum.Helmet_B:
				return 5;
			case TileEnum.Sword_B:
				return 4;
			case TileEnum.Coin_3_B:
				return 3;
			case TileEnum.Coin_2_B:
				return 2;
			case TileEnum.Coin_1_B:
				return 1;
			default:
				return 0;
		}
	}

	public static int GetOrderRed(TileEnum tileType)
	{
		switch (tileType)
		{
			case TileEnum.Helmet_R:
				return 5;
			case TileEnum.Sword_R:
				return 4;
			case TileEnum.Coin_3_R:
				return 3;
			case TileEnum.Coin_2_R:
				return 2;
			case TileEnum.Coin_1_R:
				return 1;
			case TileEnum.Helmet_B:
				return -5;
			case TileEnum.Sword_B:
				return -4;
			case TileEnum.Coin_3_B:
				return -3;
			case TileEnum.Coin_2_B:
				return -2;
			case TileEnum.Coin_1_B:
				return -1;
			default:
				return 0;
		}
	}
}
