﻿using System.Collections;
using System.Collections.Generic;

public enum TileEnum
{
    Blank,
    Helmet_B,
    Helmet_R,
    Sword_B,
    Sword_R,
    Coin_1_B,
    Coin_1_R,
    Coin_2_B,
    Coin_2_R,
    Coin_3_B,
    Coin_3_R,
    Empty
}
