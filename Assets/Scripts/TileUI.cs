﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TileUI : MonoBehaviour
{
	public SetupBoard board;
    public Tile tile;
	public Tuple<int, int> tileCoords;
	public Material blank;
	public Material sword_black;
	public Material sword_red;
	public Material helmet_black;
	public Material helmet_red;
	public Material coin_1_black;
	public Material coin_1_red;
	public Material coin_2_black;
	public Material coin_2_red;
	public Material coin_3_black;
	public Material coin_3_red;
	// Start is called before the first frame update
	void Start()
    {
		RefreshDisplay();
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void Collect(Player collector, Player enemy)
    {
		if (tile.tileType == TileEnum.Blank)
        {
			foreach(GameObject tileGameObject in board.board)
            {
				TileUI tileUI = tileGameObject.GetComponent<TileUI>();
				if (tileUI.tile.tileType == TileEnum.Blank)
                {
					tileUI.tile.Collect(collector, enemy);
					tileUI.RefreshDisplay();
				}
			}

			board.KaitoLocation = new Tuple<int, int>(tileCoords.Item1, tileCoords.Item2);
		} else if (board.KaitoLocation != null && tile.tileType != TileEnum.Empty && (tileCoords.Item1 == board.KaitoLocation.Item1 || tileCoords.Item2 == board.KaitoLocation.Item2))
        {
			tile.Collect(collector, enemy);
			RefreshDisplay();
			board.KaitoLocation = tileCoords;
		}
	}

	public void BringBackSword(Player player)
    {
		Tile tile;
		if (player.isRed)
		{
			tile = new Tile(TileEnum.Sword_R, player);
		} else
        {
			tile = new Tile(TileEnum.Sword_B, player);
		}


		this.tile = tile;
		RefreshDisplay();
		player.Swords++;
		player.CoinsCollected -= 4;
		player.Score += 40;
		board.KaitoLocation = tileCoords;
	}

	public void BringBackHelmet(Player player)
	{
		Tile tile;
		if (player.isRed)
		{
			tile = new Tile(TileEnum.Helmet_R, player);
		}
		else
		{
			tile = new Tile(TileEnum.Helmet_B, player);
		}

		this.tile = tile;
		RefreshDisplay();
		player.Helmets++;
		player.Score += 120;
		player.CoinsCollected -= 5;
		board.KaitoLocation = tileCoords;
	}

	private void RefreshDisplay()
    {
		MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		
		if (tile.tileType == TileEnum.Empty)
		{
			meshRenderer.enabled = false;
		}
        else
        {
			meshRenderer.material = GetMaterial(tile.tileType);
			if (!meshRenderer.enabled)
			{
				meshRenderer.enabled = true;
			}
		}
	}

	private Material GetMaterial(TileEnum tile)
	{
		switch (tile)
		{
			case TileEnum.Blank:
				return blank;
			case TileEnum.Sword_B:
				return sword_black;
			case TileEnum.Sword_R:
				return sword_red;
			case TileEnum.Helmet_B:
				return helmet_black;
			case TileEnum.Helmet_R:
				return helmet_red;
			case TileEnum.Coin_1_B:
				return coin_1_black;
			case TileEnum.Coin_1_R:
				return coin_1_red;
			case TileEnum.Coin_2_B:
				return coin_2_black;
			case TileEnum.Coin_2_R:
				return coin_2_red;
			case TileEnum.Coin_3_B:
				return coin_3_black;
			case TileEnum.Coin_3_R:
				return coin_3_red;
			default:
				return blank;
		}
	}
}
